from io import BytesIO
import json
import uuid
import numpy as np
import pandas as pd
from PIL import Image

import app.definitions as defn


class FourierApproximator:
    def __init__(self, image_buffer: Image ,cutoff_coefficient: int):
        self.image_buffer = image_buffer
        self.cutoff_coefficient = cutoff_coefficient
        self._STATIC_WIDTH = defn.STATIC_WIDTH
        self._STATIC_HEIGHT = defn.STATIC_HEIGHT
 
    def crop_image(self):
        try:
            image_object = Image.open(self.image_buffer)
            image_object.resize((self._STATIC_WIDTH, self._STATIC_HEIGHT), Image.NEAREST)
        except IOError:
            print("IOError – the file cannot be found, or the image cannot be opened and identified")
            return None 
        signal = np.array(image_object.convert(mode='L'))
        image_height,image_width = signal.shape
        vertical_signal = signal.mean(axis=0)
        if self.cutoff_coefficient > 0:
            rolling_window = (pd.Series(vertical_signal).rolling(self.cutoff_coefficient)
            .mean()
            .fillna(np.average(vertical_signal)-.25*np.std(vertical_signal)))
            thresh = rolling_window.describe()['min'] == rolling_window
            min_val = rolling_window[thresh].index[0]
            filepath = '/tmp/cropped-{0}.png'.format(uuid.uuid1())
            try:
                image_object.crop((0,min_val,image_width, image_height)).save(filepath)
            except IOError:
                print("IOError – the file could not be written. The file may have been created, and may contain partial data.")
                return None
            return filepath
        else:
            print("Cutoff coefficient not properly indentified") # insert logging here
            return None


class DualFourierApproximator:
    def __init__(self, image_buffer: Image, cutoff_coefficient: int):
        self.image_buffer = image_buffer
        self.cutoff_coefficient = cutoff_coefficient
        self._STATIC_HEIGHT = defn.STATIC_HEIGHT
        self._STATIC_WIDTH = defn.STATIC_WIDTH

    def crop_image(self):
        try:
            image_object = Image.open(self.image_buffer)
            image_object.resize((self._STATIC_WIDTH, self._STATIC_HEIGHT), Image.NEAREST)
        except IOError:
            print("IOError – the file cannot be found, or the image cannot be opened and identified")
            return None 
        signal = np.array(image_object.convert(mode='L'))
        image_height,image_width = signal.shape
        vertical_signal = signal.mean(axis=0)
        if self.cutoff_coefficient > 0:
            rolling_window = (pd.Series(vertical_signal).rolling(self.cutoff_coefficient)
            .mean()
            .fillna(np.average(vertical_signal)-.25*np.std(vertical_signal)))
            thresh = rolling_window.describe()['min'] == rolling_window
            min_val = rolling_window[thresh].index[0]
            filepath = '/tmp/cropped-{0}.png'.format(uuid.uuid1())
            try:
                image_object.crop((0,min_val,image_width, image_height)).save(filepath)
            except IOError:
                print("IOError – the file could not be written. The file may have been created, and may contain partial data.")
                return None
            return filepath
        else:
            print("Cutoff coefficient not properly indentified") # insert logging here
            return None

        
class DefaultImage:
    def __init__(self, image_buffer: Image):
        self.image_buffer = image_buffer
        self._unit_test_passed = False
    
    def crop_image(self):
        try:
            unit_test_image = Image.open(self.image_buffer)
        except IOError:
            print("IOError – the file could not be written. The file may have been created, and may contain partial data.")
            return None
        filepath = '/tmp/unit_test-{0}.png'.format(uuid.uuid1())
        signal = np.array(unit_test_image.convert(mode='L'))
        image_height,image_width = signal.shape
        vertical_signal = signal.mean(axis=0)
        unit_test_image.crop((0,0,image_width, image_height)).save(filepath)
        return filepath


# class VerticalWhitespace:
#     def __init__(self, image, percentile, use_num_groups, num_groups, cutoff_value, target_idx):
#         self.image_buffer = image_buffer
#         self.flattened_signal = np.array(self.image_buffer.convert(mode='L'))
#         # model specific parameters
#         self.percentile = percentile
#         self.use_num_groups = use_num_groups
#         self.num_groups = num_groups
#         self.cutoff_value = cutoff_value
#         self.target_idx = target_idx

#         self.histogram_data = []
#         self.image_df = pd.DataFrame()
#         self.image_array = None
    
# def fit(self):
#         # calculate the average pixel value BY row
#         self.histogram_data = [sum(self.flattened_signal[i])/self.width for i in range(self.height)]
#         self.image_df['average_row_value'] = self.histogram_data
#         pixel_array = np.array(self.image_df['average_row_value']).reshape(-1, 1)
#         self.image_df['normalized_average_row_value'] = (pixel_array - min(pixel_array)) / (max(pixel_array) - min(pixel_array))

#         np.percentile(self.image_df['average_row_value'], 50)
#         percentile = np.quantile(self.image_df['average_row_value'], self.percentile)

#         self.image_df.loc[self.image_df.average_row_value >= percentile, 'group'] = "whitespace"
#         self.image_df.loc[self.image_df.average_row_value < percentile, 'group'] = "text"

#         # here is the sauce that links the whitespace together by index
#         whitespace_idx_ranges = []
#         counter = 0
#         last_seen_whitespace = 0
#         last_seen_text = 0
    
#         # we traverse 'group' and output list of lists (whitespace_idx_ranges) with the list having two values:
#         # [[val1, val2], [val3, val4], ...] constraints val1 < val2, val3 < val4, and so on.
#         # each of the inner lists represents a row cutoff that is eventually used to split the image
#         for i, val in enumerate(self.image_df['group']):
#             if val == 'whitespace' and i != len(self.image_df['group']) - 1:  # avoid out of bounds error
#                 last_seen_whitespace = i
#                 if counter == 0:
#                     start_idx = i
#                 counter += 1
#             elif val == 'text':
#                 last_seen_text = i
#                 if self.image_df['group'][i - 1] == 'whitespace':
#                     whitespace_idx_ranges.append([start_idx, i - 1])
#                 counter = 0
#             elif i == len(self.image_df['group']) - 1:  # address out of bounds error here
#                 if val == 'whitespace':
#                     last_seen_whitespace = i
#                 if last_seen_whitespace == i:
#                     whitespace_idx_ranges.append([last_seen_text + 1, last_seen_whitespace])

#         # append difference of the indices calculated above (the bigger the difference, the bigger the whitespace)
#         for val in whitespace_idx_ranges:
#             val.append(np.subtract(val[1], val[0]))
    
#         # currently, user can specify how many groups they want
#         # OR it can be determined by 'vertical_whitespace_diff' in FILE_CONFIGS
#         if self.use_num_groups is False:
#             large_whitespace = [val for val in whitespace_idx_ranges
#                                 if val[2] >= self.cutoff_value]
#             large_whitespace.append([self.height, None])
#         else:
#             large_whitespace = sorted(whitespace_idx_ranges, key=itemgetter(2), reverse=True)[0:self.num_groups]
#             large_whitespace = sorted(large_whitespace, key=itemgetter(0))
#             large_whitespace.append([self.height, None])

#         final_idxs = []
#         for i, val in enumerate(large_whitespace):
#             if i + 1 < len(large_whitespace):
#                 final_idxs.append([val[1], large_whitespace[i+1][0]])

#         final_idxs = list(map(splice_utility.alter_endpoints_by_delta, final_idxs))
#         final_idxs[-1][1] = len(self.image_df)

#         if len(final_idxs) > 0:
#             final_idx = final_idxs[self.target_idx]
#             self.output_image = self.image.crop((0, final_idx[0], self.width, final_idx[1]))

#             if defn.SHOW_GRAPHS is True:
#                 self.output_image.show()

#             return 1, self.output_image
#         else:
#             # block for when VerticalWhitespace fails
#             self.logger.warning("VerticalWhitespace failed.")
#             return 0, None


        


