JACKSON_MEMORIAL = 150
STATIC_WIDTH = 1566
STATIC_HEIGHT = 2048
# DOC_SHAPE_IDS = ['204_jackson_memorial', '107_mayo_clinic', '120_baptist_health']
DOC_SHAPE_INFO = {
    '204_jackson_memorial': ['fourier',150],
    '107_mayo_clinic': ['fourier',113],
    '120_baptist_health': ['fourier',680]
}