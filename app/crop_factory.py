import json

import app.definitions as app_defn
from app.crop import FourierApproximator 
from app.crop import DefaultImage


class CropFactory:
    
    def __init__(self, image_buffer, abstract_type, document_shape, doc_cutoff):
        self.concrete_crop = None
        if abstract_type == app_defn.DOC_SHAPE_INFO[document_shape][0]:
            self.concrete_crop = FourierApproximator(image_buffer, doc_cutoff)
        elif abstract_type == 'unit_test':
            self.concrete_crop = DefaultImage(image_buffer)

    def concrete_crop_image(self):
        concrete_impl = self.concrete_crop.crop_image()
        if concrete_impl == None:
            return -1
        return concrete_impl