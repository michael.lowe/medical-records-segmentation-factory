import json
import uuid
import logging
import connexion

from flask import Flask, jsonify, make_response, send_file
from connexion.resolver import RestyResolver

import app.definitions as defn
from app.crop_factory import CropFactory


def valid_response_handler(filename, seq_id, source_filepath):
    return make_response(send_file(source_filepath, as_attachment=True, attachment_filename='cropped.png'), 200)


def error_response_handler(seq_id,filename,mime_type):
    logging.error("Processed document failed. Document name: {0} MIMETYPE: {1}".format(
        filename,mime_type
    ))
    response_json =  {
        'error_message': 'processed document failed. Document format/shape not supported.',
        'filename': filename,
        'mimetype': mime_type,
        'instance_id': seq_id          
    }
    return make_response(jsonify(response_json), 503)


def post_event_handler(doc_type,doc_shape_id, doc_cutoff,secondary_doc_cutoff=None):
    seq_id = uuid.uuid1()
    doc = connexion.request.files['document']
    valid_extension = lambda abstract_file: abstract_file.mimetype
    valid_doc_shape = lambda inbound_docshape: True if inbound_docshape in defn.DOC_SHAPE_INFO.keys() else False
    if valid_extension(doc) == 'image/png' and valid_doc_shape(doc_shape_id):
        return post_controller(doc, doc_shape_id,doc_type,seq_id, doc_cutoff)
    return error_response_handler(seq_id, doc.filename,doc.mimetype)
 

def post_controller(doc,doc_shape_id,doc_type,seq_id,doc_cutoff,secondary_doc_cutoff=None):
    con_crop = CropFactory(doc,str(doc_type), doc_shape_id, doc_cutoff)
    dest_filepath = con_crop.concrete_crop_image()
    if dest_filepath:
        return valid_response_handler(doc.filename, seq_id, dest_filepath)
    else:
        return error_response_handler(seq_id, doc.filename,doc.mimetype)


if __name__ == "__main__":
    app = connexion.App(__name__, specification_dir='swagger/')
    app.add_api('app.yml',resolver=RestyResolver('api'))
    app.run(port=8080)
    logging.info("Running Application Server on Port 8080")
